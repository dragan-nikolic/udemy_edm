# udemy_edm

Udemy Ethereum Developer Mastercalss course

# Setup Development Environment

## Install 'geth'
- https://geth.ethereum.org/downloads/
- Make sure to install Development tools too
- Installed v1.7.3

## Setup private Ethereum instance with geth

- Create folder that holds db and accounts of private instance (on Windows)
  Let's assume you create \ethereum\chaindata on C drive
PS> mkdir \ethereum\chaindata

- Create genesis.json file in the parent folder (i.e. in \ethereum)
--- genesis.json content ---
{
 "coinbase": "0x0000000000000000000000000000000000000001",
 "difficulty": "0x20000",
 "extraData": "",
 "gasLimit": "0x8000000",
 "nonce": "0x0000000000000042",
 "mixhash": "0x0000000000000000000000000000000000000000000000000000000000000000",
 "parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
 "timestamp": "0x00",
 "alloc": {},
 "config": {
    "chainId": 4224,
    "homesteadBlock": 0,
    "eip155Block": 0,
    "eip158Block": 0
 }
}
----------------------------

- Initialize private instance (chain)
PS> pushd \ethereum
PS> geth --datadir c:\ethereum\chaindata init genesis.json

If everything ok we should see: 'Successfully wrote genesis state'
and following directories should be created under 'private' dir:
- geth
- keystore

- Create 3 accounts
Use following command 3 times. For password use 'Pass321':
PS> geth --datadir c:\ethereum\chaindata account new

Check tha accounts are created with:
PS> tree . /F
or
PS> geth --datadir c:\ethereum\chaindata account list

Note: Accounts private keys should be carefully protected.

- Create startup script
PS> code startnode.cmd
--- simple startnode.cmd ---
geth ^
  --networkid 4224 ^
  --datadir c:\ethereum\chaindata ^
  --nodiscover ^
  --rpc ^
  --rpcport "8545" ^
  --port "30303" ^
  --rpccorsdomain "*" ^
  --nat "any" ^
  --rpcapi eth,web3,personal,net
---------------------

--- full startnode.cmd ---
geth ^
  --networkid 4224 ^
  --mine ^
  --datadir c:\ethereum\chaindata ^
  --nodiscover ^
  --rpc ^
  --rpcport "8545" ^
  --port "30303" ^
  --rpccorsdomain "*" ^
  --nat "any" ^
  --rpcapi eth,web3,personal,net ^
  --unlock 0 ^
  --password c:\ethereum\chaindata\password.sec
---------------------


## Install MIST

- Go to https://github.com/ethereum/mist/releases
- Windows installation package: Mist-installer-0-9-3.exe
- start private network using geth
- start mist
  > opens and shows 3 accounts

## mine some ether

Windows:
- open another console and type 'geth attach'
  > geth console gets open
- start mining by typing: 'miner.start(1)'
- stop mining by typing: 'mioner.stop()'
